


* For the ASA labs, use the following pod information:

| Pod Number |  Session Name |  Usernames |  Password |  Public IPs   |
| --- | --- | --- | --- | --- |
| 1 | 30 - asa30 | cisco | C1sco12345 | 173.37.56.93:5000 |
| 2 | 31 - asa31 | cisco | C1sco12345 | 173.37.56.93:5001 |
| 3 | 32 - asa32 | cisco | C1sco12345 | 173.37.56.93:5002 |
| 4 | 33 - asa33 | cisco | C1sco12345 | 173.37.56.93:5003 |
| 5 | 34 - asa34 | cisco | C1sco12345 | 173.37.56.93:5004 |
| 6 | 35 - asa35 | cisco | C1sco12345 | 173.37.56.93:5005 |
| 7 | 36 - asa36 | cisco | C1sco12345 | 173.37.56.93:5006 |
| 8 | 37 - asa37 | cisco | C1sco12345 | 173.37.56.93:5007 |
| 9 | 38 - asa38 | cisco | C1sco12345 | 173.37.56.93:5008 |
| 10 | 39 - asa39 | cisco | C1sco12345 | 173.37.56.93:5009 |
| 11 | 40 - asa40 | cisco | C1sco12345 | 173.37.56.94:5000 |
| 12 | 41 - asa41 | cisco | C1sco12345 | 173.37.56.94:5001 |
| 13 | 42 - asa42 | cisco | C1sco12345 | 173.37.56.94:5002 |
| 14 | 43 - asa43 | cisco | C1sco12345 | 173.37.56.94:5003 |
| 15 | 44 - asa44 | cisco | C1sco12345 | 173.37.56.94:5004 |
| 16 | 45 - asa45 | cisco | C1sco12345 | 173.37.56.94:5005 |
| 17 | 46 - asa46 | cisco | C1sco12345 | 173.37.56.94:5006 |
| 18 | 47 - asa47 | cisco | C1sco12345 | 173.37.56.94:5007 |
| 19 | 48 - asa48 | cisco | C1sco12345 | 173.37.56.94:5008 |
| 20 | 49 - asa49 | cisco | C1sco12345 | 173.37.56.94:5009 |

* In your pod, change into `ansible-asa` folder and edit `hosts` file. Uncomment the line with your assigned ASA under the `[asas]` group.
  * Be sure to not uncomment additional lines, or you may have config conflicts with other attendees